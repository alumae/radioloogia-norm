SHELL = /bin/bash
LM_EE=/home/tanel/devel/lm_ee
ET_G2P=/home/tanel/devel/et-g2p/run.sh

# don't delete intermediate files
.SECONDARY:
.DELETE_ON_ERROR:

VALID_TYPES=KT RG UH MR ES MG AG EN OP


build/tok/%.txt: build/source/%.txt grammars/tokenizer/tokenizer.far
	mkdir -p `dirname $@`
	cat build/source/$*.txt  | \
	grep -v "^###" | \
	thraxrewrite-tester --far=grammars/tokenizer/tokenizer.far --rules=TOKENIZER  | cut -d " " -f 5- > $@

build/recaser.model: build/tok/norm.train.txt
	python recaser/recaser.py --train build/tok/norm.train.txt --save $@


build/recased/%.txt: build/tok/%.txt build/recaser.model
	mkdir -p `dirname $@`
	python recaser/recaser.py --load build/recaser.model --apply build/tok/$*.txt > $@


build/recased/pron-s1.%.txt: build/recased/pron.%.txt
	cat $^ | \
	grep -v "^###" | \
	$(LM_EE)/scripts/num-infl.py  -m $(LM_EE)/build/num-infl/model.pkl > $@


###

build/recased-orig2norm/%.txt: build/recased/%.txt
	mkdir -p `dirname $@`
	cat $^ | \
	thraxrewrite-tester --far=grammars/orig2norm/orig2norm.far --rules=expander,BytesToChars,CharLM,CharsToBytes |  \
	cut -d " " -f 5- > $@


build/recased/norm2pron-s1.%.txt: build/recased/norm.%.txt
	cat build/recased/norm.$*.txt | \
	thraxrewrite-tester --far=grammars/norm2pron-s1/norm2pron-s1.far --rules=NORM2PRON_S1_EXPAND,BytesToChars,CharLM,CharsToBytes |  \
	cut -d " " -f 5- > $@
	
build/recased-orig2norm2pron-s1/%.txt: build/recased-orig2norm/%.txt
	mkdir -p `dirname $@`
	cat $^ | \
	thraxrewrite-tester --far=grammars/norm2pron-s1/norm2pron-s1.far --rules=NORM2PRON_S1_EXPAND,BytesToChars,CharLM,CharsToBytes |  \
	cut -d " " -f 5- > $@


build/recased-orig2norm2pron-s1/unannotated-train/.meta: $(addprefix build/recased-orig2norm2pron-s1/unannotated-train/, $(addsuffix .txt,$(VALID_TYPES) XX)) 


build/recased-orig2norm2pron-s1/dev/.meta: $(addprefix build/recased-orig2norm2pron-s1/dev/, $(addsuffix .txt,$(VALID_TYPES) XX)) 

build/recased-orig2norm2pron-s1/test/.meta: $(addprefix build/recased-orig2norm2pron-s1/test/, $(addsuffix .txt,$(VALID_TYPES) XX)) 

build/recased-orig2norm2pron-s1/spoken-test1-by-mod/.meta: $(addprefix build/recased-orig2norm2pron-s1/spoken-test1-by-mod/, $(addsuffix .txt,$(VALID_TYPES))) 

##

build/lm/recased-pron-s1/orig2norm2pron-s1.counts1: $(addprefix build/recased-orig2norm2pron-s1/unannotated-train/, $(addsuffix .txt,$(VALID_TYPES) XX)) 
	cat $^ | ngram-count -text - -order 1 -write $@


build/lm/recased-pron-s1/orig2norm2pron-s1.vocab0: build/lm/recased-pron-s1/orig2norm2pron-s1.counts1
	echo "," > $@
	cat $^ | sort -nr -k 2 | grep -v -P '\t1$$' | awk '{print($$1)}' | \
		grep -v -P "\d" | \
		grep -P -v "<.?s>" | \
		grep -v "," | \
		egrep -v "\." \
		>> $@

build/lm/recased-pron-s1/orig2norm2pron-s1.dict: build/lm/recased-pron-s1/orig2norm2pron-s1.vocab0 data/punctuation.txt
	sort -k 2b,2 data/punctuation.txt > punctuation-sorted.txt
	cat punctuation-sorted.txt | perl -npe 's/\S+\s//' |$(ET_G2P)  | join -1 2 punctuation-sorted.txt - | perl -npe 's/\S+\s//' > $@
	cat build/lm/recased-pron-s1/orig2norm2pron-s1.vocab0 | $(ET_G2P) >> $@
	rm punctuation-sorted.txt

build/lm/recased-pron-s1/orig2norm2pron-s1.vocab: build/lm/recased-pron-s1/orig2norm2pron-s1.dict
	cat $^ | perl -npe 's/(\(\d\))?\s+.*//' | uniq > $@


%.with_long.dict: %.dict
	cat $^ |  perl -npe 's/\b(\w+) \1\b/\1\1/g; s/(\s)jj\b/\1j/g;' > $@

build/lm/recased-pron-s1/orig2norm2pron-s1.4g.arpa.gz: build/lm/recased-pron-s1/orig2norm2pron-s1.vocab
	cat $(addprefix build/recased-orig2norm2pron-s1/unannotated-train/, $(addsuffix .txt,$(VALID_TYPES) XX)) | \
	ngram-count -text - -vocab build/lm/recased-pron-s1/orig2norm2pron-s1.vocab \
		-order 4 -kndiscount -interpolate -lm - | make-hiddens-lm | perl -npe 's/<#s>/<newline>/g' | gzip -c > $@





### LM ###

build/lm/recased-norm/train.txt: build/recased/norm.train.txt
	cat $^ | grep -v "^#" > $@

build/lm/recased-norm/train.vocab: build/lm/recased-norm/train.txt
	mkdir -p build/lm/recased-norm 
	cat $^ | perl -npe 's/ /\n/g' | sort | uniq -c | sort -nr -k 1 | grep -v " 1 " | perl -npe 's/^\s+\d+ //' > $@
	cat $^ | perl -npe 's/ /\n/g' | sort | uniq -c | sort -nr -k 1 | grep  " 1 " | sed -n '1~2!p' | perl -npe 's/^\s+\d+ //' >> $@
	
build/lm/recased-norm/train.syms: build/lm/recased-norm/train.vocab
	mkdir -p build/lm/recased-norm
	cat $^ | ngramsymbols > $@
	
build/lm/recased-norm/train.far: build/lm/recased-norm/train.txt build/lm/recased-norm/train.syms
	farcompilestrings -symbols=build/lm/recased-norm/train.syms -keep_symbols=1 --unknown_symbol='<unk>' build/lm/recased-norm/train.txt > $@
	
build/lm/recased-norm/train.cnts: build/lm/recased-norm/train.far
	ngramcount -order=4 $^ > $@
	
build/lm/recased-norm/train.mod: build/lm/recased-norm/train.cnts
	 ngrammake --method=kneser_ney build/lm/recased-norm/train.cnts > build/lm/recased-norm/train.mod

build/lm/recased-norm/train.no-syms.mod: build/lm/recased-norm/train.mod
	fstsymbols --clear_isymbols ---clear_osymbols $^ > $@
	 
build/lm/recased-norm/train.alpha: build/lm/recased-norm/train.vocab
	 cat $^ | perl -npe 's/(.*)/\1\t\1/' > $@


# char lm
build/lm/recased-norm-char/train.txt: build/recased/norm.train.txt
	mkdir -p build/lm/recased-norm-char 
	cat $^ | grep -v "^#" | perl -npe 'use encoding 'utf8'; s/ /_/g; s/(.)/\1 /g;'  > $@

build/lm/recased-norm-char/train.vocab: build/lm/recased-norm-char/train.txt
	mkdir -p build/lm/recased-norm-char 
	cat $^ | perl -npe 's/ /\n/g' | sort | uniq  > $@
	
build/lm/recased-norm-char/train.syms: build/lm/recased-norm-char/train.vocab
	mkdir -p build/lm/recased-norm-char
	cat $^ | ngramsymbols > $@
	
build/lm/recased-norm-char/train.far: build/lm/recased-norm-char/train.txt build/lm/recased-norm-char/train.syms
	farcompilestrings -symbols=build/lm/recased-norm-char/train.syms -keep_symbols=1 --unknown_symbol='<unk>' build/lm/recased-norm-char/train.txt > $@
	
build/lm/recased-norm-char/train.cnts: build/lm/recased-norm-char/train.far
	ngramcount -order=20 $^ > $@
	
build/lm/recased-norm-char/train.mod: build/lm/recased-norm-char/train.cnts
	 ngrammake --method=kneser_ney build/lm/recased-norm-char/train.cnts > build/lm/recased-norm-char/train.mod

build/lm/recased-norm-char/train.pruned.mod: build/lm/recased-norm-char/train.mod
	ngramshrink -method=relative_entropy --theta=1.0e-7  $^  > $@

%.no-syms.mod: %.mod
	fstsymbols --clear_isymbols ---clear_osymbols $^ > $@
 
build/lm/recased-norm-char/train.alpha: build/lm/recased-norm-char/train.vocab
	 cat $^ | perl -npe 's/(.*)/\1\t\1/' > $@

## RECASED-PRON-S1

build/lm/recased-pron-s1/train.vocab: build/recased/pron-s1.train.txt
	mkdir -p `dirname $@`
	cat $^ | perl -npe 's/ /\n/g' | sort | uniq -c | sort -nr -k 1 | grep -v " 1 " | perl -npe 's/^\s+\d+ //' > $@
	cat $^ | perl -npe 's/ /\n/g' | sort | uniq -c | sort -nr -k 1 | grep  " 1 " | sed -n '1~2!p' | perl -npe 's/^\s+\d+ //' >> $@
	
build/lm/recased-pron-s1/train.syms: build/lm/recased-pron-s1/train.vocab
	cat $^ | ngramsymbols > $@
	
build/lm/recased-pron-s1/train.far: build/recased/pron-s1.train.txt build/lm/recased-pron-s1/train.syms
	farcompilestrings -symbols=build/lm/recased-pron-s1/train.syms -keep_symbols=1 --unknown_symbol='<unk>'  build/recased/pron-s1.train.txt > $@
	
build/lm/recased-pron-s1/train.cnts: build/lm/recased-pron-s1/train.far
	ngramcount -order=4 $^ > $@
	
build/lm/recased-pron-s1/train.mod: build/lm/recased-pron-s1/train.cnts
	 ngrammake --method=kneser_ney build/lm/recased-pron-s1/train.cnts > $@

build/lm/recased-pron-s1/train.no-syms.mod: build/lm/recased-pron-s1/train.mod
	fstsymbols --clear_isymbols ---clear_osymbols $^ > $@
	 

build/lm/recased-pron-s1/train.alpha: build/lm/recased-pron-s1//train.vocab
	 cat $^ | perl -npe 's/(.*)/\1\t\1/' > $@



### char lm

# char lm
build/lm/recased-pron-s1-char/train.txt: build/recased/pron-s1.train.txt
	mkdir -p build/lm/recased-pron-s1-char 
	cat $^ | grep -v "^#" | perl -npe 'use encoding 'utf8'; s/ /_/g; s/(.)/\1 /g;'  > $@

build/lm/recased-pron-s1-char/train.vocab: build/lm/recased-pron-s1-char/train.txt
	mkdir -p build/lm/recased-pron-s1-char 
	cat $^ data/chars.txt | perl -npe 's/ /\n/g' | sort | uniq  > $@
	
build/lm/recased-pron-s1-char/train.syms: build/lm/recased-pron-s1-char/train.vocab
	mkdir -p build/lm/recased-pron-s1-char
	cat $^ | ngramsymbols > $@
	
build/lm/recased-pron-s1-char/train.far: build/lm/recased-pron-s1-char/train.txt build/lm/recased-pron-s1-char/train.syms build/lm/recased-pron-s1-char/train.vocab
	farcompilestrings -symbols=build/lm/recased-pron-s1-char/train.syms -keep_symbols=1 --unknown_symbol='<unk>' build/lm/recased-pron-s1-char/train.txt build/lm/recased-pron-s1-char/train.vocab $@
	
build/lm/recased-pron-s1-char/train.cnts: build/lm/recased-pron-s1-char/train.far
	ngramcount -order=20 $^ > $@
	
build/lm/recased-pron-s1-char/train.mod: build/lm/recased-pron-s1-char/train.cnts
	 ngrammake  --method=kneser_ney build/lm/recased-pron-s1-char/train.cnts > build/lm/recased-pron-s1-char/train.mod

build/lm/recased-pron-s1-char/train.pruned.mod: build/lm/recased-pron-s1-char/train.mod
	ngramshrink -method=relative_entropy --theta=1.0e-7  $^  > $@

build/lm/recased-pron-s1-char/train.alpha: build/lm/recased-pron-s1-char/train.vocab
	 cat $^ | perl -npe 's/(.*)/\1\t\1/' > $@

######################

build/%.ids: build/source/orig.%.txt
	grep "###" $^ | perl -npe 's/.*ARTIKKEL //; s/ //g;' | grep -v "^$$" > $@
	
build/unannotated/all.csv:
	rm -f $@
	for f in data/orig/*.xlsx; do \
		ssconvert --export-type=Gnumeric_stf:stf_csv $$f tmp.csv; \
		cat tmp.csv >> $@; \
		rm tmp.csv; \
	done
	
build/source/unannotated-train: build/unannotated/all.csv build/dev.ids build/test.ids
	rm -rf $@
	mkdir -p $@
	./scripts/parse_unannotated.py -t "$(VALID_TYPES)" -x build/dev.ids -x build/test.ids $@ build/unannotated/all.csv 

build/source/dev: build/unannotated/all.csv build/dev.ids
	rm -rf $@
	mkdir -p $@
	./scripts/parse_unannotated.py -t "$(VALID_TYPES)" -i build/dev.ids $@ build/unannotated/all.csv 

build/source/test: build/unannotated/all.csv build/test.ids
	rm -rf $@
	mkdir -p $@
	./scripts/parse_unannotated.py -t "$(VALID_TYPES)" -i build/test.ids $@ build/unannotated/all.csv 


###########################

build/source/spoken-test1-by-mod: data/spoken-test1
	rm -rf $@
	mkdir -p $@
	for t in $(VALID_TYPES); do \
		find data/spoken-test1/ -iname "*hper$$t*" | xargs cat > $@/$$t.txt; \
	done
	


####################

build/charred/%.txt: build/recased/%.txt 
	mkdir -p build/charred
	cat $^| \
	perl -npe 'BEGIN {binmode STDIN, "utf8"; binmode STDOUT, "utf8"; use utf8;} s/\n/\$$/g; s/###.*?\$$/\n/; s/ +/_/g; s/(\X)/\1 /gu;' > $@
