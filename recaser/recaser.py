#! /usr/bin/python
# -*- coding: utf-8 -*-

import sys
import logging

from optparse import OptionParser
import numpy as np
import codecs

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Perceptron
from sklearn.naive_bayes import MultinomialNB
from sklearn import svm
import cPickle
import gzip

def make_train_data(filename):
  train_words = []
  train_classes = []
  if filename.endswith(".gz"):
    f = gzip.open(filename, "r")
  else:
    f = open(filename, "r")
  reader = codecs.getreader("utf-8")    
  for l in reader(f):
    if not l.startswith("#"):
      words = l.split()
      if len(words) > 1:
        skip_next = True
        for word in words:
          if word in [".", "-", "=", ":"]:
            skip_next = True
            continue
          
          if word == word.lower():
            train_words.append(" %s " % word)
            train_classes.append("L")
          elif word == word.upper():
            train_words.append(" %s " % word)
            train_classes.append("U")
          else:
            if not skip_next:
              if word == word.capitalize():
                train_words.append(" %s " % word)
                train_classes.append("C")
              else:              
                train_words.append(" %s " % word)
                train_classes.append("M")
          skip_next = False
          
  return train_words, train_classes
            
def apply(word, cl):
  # skipime UH-uuring, aga mitte R�-GR
  if not word == word.upper():
    parts = word.partition("-")
    if parts[-1] != "" and parts[0].upper() == parts[0]:
      return word
  if cl == "L":
    return word.lower()
  elif cl == "U":
    return word.upper()
  elif cl == "C":
    return word.capitalize()
  else:
    return word


# Display progress logs on stdout
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')
                    
op = OptionParser()

op.add_option("--train",
              action="store", dest="train_filename",
              help="Train from text file")
op.add_option("--test",
              action="store", dest="test_filename",
              help="Test on data from text file")
op.add_option("--apply",
              action="store", dest="apply_filename",
              help="Apply model on text file")
op.add_option("--save",
              action="store", dest="save_filename",
              help="Save model on  file")
op.add_option("--load",
              action="store", dest="load_filename",
              help="Load model from file")
              
              
              
(opts, args) = op.parse_args()

if len(args) > 0:
  op.error("this script takes no arguments.")
  sys.exit(1)

clf = None
vectorizer = None
  
if opts.train_filename:
  print >> sys.stderr, "Reading training data"
  X, y = make_train_data(opts.train_filename)

  print >> sys.stderr, "Preparing training data..."
  #vectorizer = CountVectorizer(ngram_range=(1, 3), analyzer='char')
  vectorizer = TfidfVectorizer(sublinear_tf=True, ngram_range=(1, 3), max_df=0.8, analyzer='char')
  X_train = vectorizer.fit_transform(X)
  #import pdb
  #pdb.set_trace()
  
  print >> sys.stderr, "Prepared training data of shape ", X_train.shape
  clf = MultinomialNB(alpha=.01)
  #clf = svm.SVC()
  #clf = LogisticRegression(C=1, penalty='l1')
  #clf = Perceptron(n_iter=50)
  print >> sys.stderr, "Training model..."
  clf.fit(X_train, y)
  print >> sys.stderr, "Saving model.."
  if opts.save_filename:
    with open(opts.save_filename, 'wb') as fid:
      cPickle.dump((clf, vectorizer), fid)      

if opts.load_filename:
  with open(opts.load_filename, 'rb') as fid:
    (clf, vectorizer) = cPickle.load(fid)  
  
  
if opts.test_filename:
  if not clf:
    print >> sys.stderr, "No model to use: train or load from file!"
    sys.exit(1)
  if not vectorizer:
    print >> sys.stderr, "No vectorizer to use: train or load from file!"
    sys.exit(1)
    
  correct = 0
  total = 0
  print "%20s %20s %20s %s" % ("orig", "ref", "hyp", "cl")
  for l in codecs.open(opts.test_filename, "r", "utf-8"):
    l = l.strip()
    if len(l) > 0:
      orig, ref = l.split()
      cl = clf.predict(vectorizer.transform([" %s " % orig]))[0]
      hyp = apply(orig, cl)
      print "%20s %20s %20s %s" % (orig, ref, hyp, cl),
      total += 1
      if hyp == ref:
        correct += 1
        print
      else:
        print "E"
  print "Correct: ", 1.0 * correct/total 


if opts.apply_filename:
  sys.stdout = codecs.getwriter('utf8')(sys.stdout)
  if not clf:
    print >> sys.stderr, "No model to use: train or load from file!"
    sys.exit(1)
  if not vectorizer:
    print >> sys.stderr, "No vectorizer to use: train or load from file!"
    sys.exit(1)
  if opts.apply_filename.endswith(".gz"):
    f = gzip.open(opts.apply_filename, "r")
  else:
    f = open(opts.apply_filename, "r")
  reader = codecs.getreader("utf-8")    
  for l in reader(f):   
    if l.startswith("#"):
      print l,
    else:
      words = l.split()
      if len(words) > 1:
        apply_next = True
        for word in words:
          if word in [".", "-", "=", ":", "?", "\"", "("]:
            apply_next = True
          elif apply_next or (word.upper() == word and len(word) >= 2):
            # don't process mixed-case words
            if word.upper() == word or word.capitalize() == word:
              cl = clf.predict(vectorizer.transform([" %s " % word]))[0]
              word =  apply(word, cl)
            if word.endswith("."):
              apply_next = True
            else:
              apply_next = False
          print word,
          
        print
            
            
        
      
