#!/bin/bash
thraxmakedep orig2norm.grm
make
diff \
<(cat test_data/orig2norm.txt | perl -npe 's/.*\|(.*)\|.*/\1/' |  thraxrewrite-tester --far=orig2norm.far --rules=expander,BytesToChars,CharLM,CharsToBytes |  cut -d " " -f 5-) \
<(cat test_data/orig2norm.txt | perl -npe 's/.*\|.*\|(.*)/\1/')


