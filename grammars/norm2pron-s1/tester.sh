#!/bin/bash
thraxmakedep norm2pron-s1.grm
make
wdiff \
<(cat test_data/norm2pron-s1.txt | perl -npe 's/.*\|(.*)\|.*/\1/' |  thraxrewrite-tester --far=norm2pron-s1.far --rules=NORM2PRON_S1_EXPAND,BytesToChars,CharLM,CharsToBytes |  cut -d " " -f 5-) \
<(cat test_data/norm2pron-s1.txt | perl -npe 's/.*\|.*\|(.*)/\1/')
